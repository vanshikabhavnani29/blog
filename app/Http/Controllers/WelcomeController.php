<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Tag;
use App\Post;

class WelcomeController extends Controller
{
    public function index(){
        return view('blog.index', [
            'tags' => Tag::all(),
            'categories' => Category::all(),
            'posts' => Post::simplePaginate(2)
        ]);
    }

    public function category(Category $category){
        return view('blog.index', [
            'tags' => Tag::all(),
            'categories' => Category::all(),
            'posts' => $category->posts()->simplePaginate(2)
        ]);
        
    }
    public function tag(Tag $tag){
        return view('blog.index', [
            'tags' => Tag::all(),
            'categories' => Category::all(),
            'posts' => $tag->posts()->simplePaginate(2)
        ]);
        
    }


}
