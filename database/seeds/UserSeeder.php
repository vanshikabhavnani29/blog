<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'=>'Priyanka',
            'email'=>'awatramanipriyanka1@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('12345678'),
            'role'=>'admin'
        ]);


        \App\User::create([
            'name'=>'Ria',
            'email'=>'ria@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('12345678'),
            // 'role'=>'author'
        ]);

        \App\User::create([
            'name'=>'Muskan',
            'email'=>'muskan999@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('12345678'),
            // 'role'=>'admin'
        ]);
    }
}
