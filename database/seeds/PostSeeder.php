<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = \App\Category::create(['name'=>'News']);
        $categoryDesign = \App\Category::create(['name'=>'Design']);
        $categoryTechnology = \App\Category::create(['name'=>'Technology']);
        $categoryEngineering = \App\Category::create(['name'=>'Engineering']);


        $tagCustomers = \App\Tag::create(['name'=>'customers']);
        $tagJava = \App\Tag::create(['name'=>'java']);
        $tagCoding = \App\Tag::create(['name'=>'coding']);
        $tagReading = \App\Tag::create(['name'=>'reading']);

        $post1 = \App\Post::create([
            'title'=>'Quarantine',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10,18)),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/1.jpg',
            'category_id'=>$categoryDesign->id,
            'user_id'=>2,
            'published_at'=>\Carbon\Carbon::now()->format('Y-m-d')
        ]);
        
        $post2 = \App\Post::create([
            'title'=>'Why Humans Overthink?',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10,18)),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/2.jpg',
            'category_id'=>$categoryNews->id,
            'user_id'=>3,
            'published_at'=>\Carbon\Carbon::now()->format('Y-m-d')
        ]);

        $post3 = \App\Post::create([
            'title'=>'Study On Corona',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10,18)),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/3.jpg',
            'category_id'=>$categoryTechnology->id,
            'user_id'=>1,
            'published_at'=>\Carbon\Carbon::now()->format('Y-m-d')
        ]);

        $post4 = \App\Post::create([
            'title'=>'How To Become Java Developer',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10,18)),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/4.jpg',
            'category_id'=>$categoryEngineering->id,
            'user_id'=>2,
            'published_at'=>\Carbon\Carbon::now()->format('Y-m-d')
        ]);

        $post5 = \App\Post::create([
            'title'=>'Shapes and Colors',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10,18)),
            'content'=>Faker\Factory::create()->paragraphs(rand(3,7), true),
            'image'=>'posts/1.jpg',
            'category_id'=>$categoryDesign->id,
            'user_id'=>3,
            'published_at'=>\Carbon\Carbon::now()->format('Y-m-d')
        ]);

        $post1->tags()->attach([$tagReading->id, $tagCustomers->id]);
        $post2->tags()->attach([$tagCustomers->id, $tagJava->id]);
        $post3->tags()->attach([$tagCoding->id]);
        $post5->tags()->attach([$tagReading->id, $tagCustomers->id, $tagCoding->id, $tagJava->id]);
        

    }
}
